package com.fededom.countriesapp.model;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Url;

public interface CountriesApi {

    // UN SINGLE ES UN TIPO DE OBSERVABLE QUE BRINDA RX JAVA , SOLO EMITE UN VALOR Y DESPUES TERMINA

    @GET("DevTides/countries/master/countriesV2.json")
    Single<List<CountryModel>> getCountries();


    /* Si no tengo la url para pegarle al back , puedo crear un metodo y pasar la url en tiempo de ejecucion
     *
     * EJEMPLO :*/
    @GET
    Single<Object> getObject(@Url String url);
}
