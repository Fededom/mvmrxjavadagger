package com.fededom.countriesapp.di;

import com.fededom.countriesapp.model.CountriesService;
import com.fededom.countriesapp.viewmodel.ListViewModel;

import dagger.Component;

@Component(modules = {ApiModule.class})
public interface ApiComponent {

    void inject(CountriesService service);

    void inject(ListViewModel viewModel);
}
