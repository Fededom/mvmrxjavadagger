package com.fededom.countriesapp.di;

import com.fededom.countriesapp.model.CountriesApi;
import com.fededom.countriesapp.model.CountriesService;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class ApiModule {


    private static String BASE_URL = "https://raw.githubusercontent.com/";

    @Provides
    public CountriesApi provideCountriesApi(){
       return   new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()) // Convierte lo que viene en el json de respuesta en nuestro modelo
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create()) // Convierte el json en un single , de RX JAVA
                .build()
                .create(CountriesApi.class);
    }

    @Provides
    public CountriesService provideCountriesService (){
        return CountriesService.getInstance();
    }
}
